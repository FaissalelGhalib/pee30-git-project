#import library
from tkinter import *
import socket
import random
#functions output buttons
def submit_button():
    global maxstroom
    global maxspanning
    global vermogen_setpoint_B
    global maxvermogen_B
    global snelheidssetpoint
    global maxsnelheid
    global vermogen_setpoint_T
    global vermogen_T
    
    maxstroom=(txt1.get())
    maxspanning=(txt2.get())
    vermogen_setpoint_B=(txt3.get())
    maxvermogen_B=(txt4.get())
    snelheidssetpoint=(txt5.get())
    maxsnelheid=(txt6.get())
    vermogen_setpoint_T=(txt7.get())
    vermogen_T= (txt8.get())
#    print(vermogen_T)
#    vermogen_T = 'Q' + vermogen_T
    print(vermogen_T)
    msg.configure(text="Alle waarde zijn succesvol ontvangen systeem initializatie wordt gestart!")

#def socket_communicatie
#   print("Geen vastlopen bij thread creatie gevonden.")
#
#    HOST = '0.0.0.0' # Zorg ervoor dat elk IP-adres geaccepteert wordt
#    PORT = 2000 # De port om op te luisteren
# 
#    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#        s.bind((HOST, PORT))
#        s.listen()
#     
#        while True:
#            conn, addr = s.accept()
#            with conn:
#                print('Connected by', addr)
#                #conn.sendall
#
#
#






    
def button_profiel():
    lbl.configure(text="het systeem is nu automatisch ingesteld")

def it_must_do_something():
    print("Selection:",selected.get())
    current = selected.get()
    update_textboxes_state(current)
    
def update_textboxes_state(selectedId):
    state = 'normal' if (selectedId == 1) else 'disabled'
    txt1.configure(state='normal')
    txt2.configure(state='normal')
    txt3.configure(state=state)
    txt4.configure(state='normal')
    txt5.configure(state=state)
    txt6.configure(state='normal')
    txt7.configure(state=state)
    txt8.configure(state='normal')

def make_textbox(master, x, y):
    output = Entry(master, width =10)
    output.place(x=x, y=y)
    return output

def make_radiobutton(master, text, value, variable, x, y):
    output = Radiobutton(master,text=text, value=value, variable=variable, command=it_must_do_something, font=FONT_SETTINGS)
    output.place(x=x, y=y)
    return output

def make_button(master, text, bg, fg, command, x, y):
    output = Button(master, text=text, bg=bg, fg=fg, command=command, font=FONT_SETTINGS)
    output.place(x=x, y=y)
    return output

def make_submit_button(master):
    return make_button(master, "submit", "green", "white", submit_button, 500, 250)

# def make_label(master, text, x, y, side):
def make_label(master, text, x, y):
    output = Label(master, text=text, font=FONT_SETTINGS)
    output.place(x=x, y=y)
    return output

def make_frame(master, fill, expand, h):
    output = Frame(master, height=h)
    if fill == 0:
        output.pack(expand=expand)
    else:
        output.pack(fill=fill, expand=expand)
    return output

#variable
Spanning = 'Volt'
Stroomsterkte = 'Ampère'
Vermogen = 'Watt'
Snelheid = 'Rotaties per Minuut'
FIRST_VALUE = 1
FONT_SETTINGS = ("Arial", 12)

#make window
window = Tk()
window.title("user settings")

#resize window (fit to screen)
width, height = window.winfo_screenwidth(), window.winfo_screenheight()
window.geometry('%dx%d+0+0' % (width,height))

frame1 = make_frame(window, BOTH, True, 25)

#write text in the screen.
#profiel manueel
lbl = make_label(frame1, "Welkom bij de MCU van groep 1 hoe wilt u het systeem gebruiken?", 0, 15)
#stroom (Ampère)
lbl = make_label(frame1, "Geef een waarde voor de maximale stroom.", 0, 40)
lbl = make_label(frame1, Stroomsterkte, 600, 40)
#spanning (Volt)
lbl = make_label(frame1, "Geef een waarde voor de maximale spanning.", 0, 65)
lbl = make_label(frame1, Spanning, 600, 65)
#vermogen setpoint
lbl = make_label(frame1, "Geef een waarde voor het vermogen setpoint.", 0, 90)
lbl = make_label(frame1, Vermogen, 600, 90)
#maximale vermogen
lbl = make_label(frame1, "Geef een waarde voor de maximale vermogen van de belasting.", 0, 115)
lbl = make_label(frame1, Vermogen, 600, 115)
#snelheidssetpoint
lbl = make_label(frame1, "Geef een waarde voor de snelheidssetpoint.", 0, 140)
lbl = make_label(frame1, Snelheid, 600, 140)
#maximale snelheid
lbl = make_label(frame1, "Geef een waarde voor de maximale snelheid.", 0, 165)
lbl = make_label(frame1, Snelheid, 600, 165)
#vermogen setpoint (tempratuur)
lbl = make_label(frame1, "Geef een waarde voor het vermogen setpoint(tempratuur).", 0, 190)
lbl = make_label(frame1, Vermogen, 600, 190)
#vermogen (tempratuur)
lbl = make_label(frame1, "Geef een waarde voor het maximale vermogen (tempratuur).", 0, 215)
lbl = make_label(frame1, Vermogen, 600, 215)


#radio buttons for selecting profiel/manueel
selected = IntVar()
selected.set(FIRST_VALUE)
#Label(window,textvariable=selected)
rad1 = make_radiobutton(frame1, 'manueel', "1", selected, 500, 10)
rad2 = make_radiobutton(frame1, 'profiel vermogen setpoint', "2", selected, 600, 10)
rad3 = make_radiobutton(frame1, 'profiel snelheid setpoint', "3", selected, 800, 10)
rad4 = make_radiobutton(frame1, 'profiel vermogen setpoint (tempratuur)', "4", selected, 1000, 10)

#textboxes (creation and placement)
txt1 = make_textbox(frame1, 500, 45)
txt2 = make_textbox(frame1, 500, 70)
txt3 = make_textbox(frame1, 500, 95)
txt4 = make_textbox(frame1, 500, 120)
txt5 = make_textbox(frame1, 500, 145)
txt6 = make_textbox(frame1, 500, 170)
txt7 = make_textbox(frame1, 500, 195)
txt8 = make_textbox(frame1, 500, 220)
#sets the state for all the textboxes, based on the selected value on the radiobuttons
update_textboxes_state(selected.get())

#displays a message when the submit button is clicked
msg = make_label(frame1, '', 400, 400)

#make buttons
make_submit_button(frame1)


##repeat the window so it stays on the screen
window.mainloop()
