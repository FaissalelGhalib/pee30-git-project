#import library
from tkinter import *
import socket
import time
from tkinter import messagebox

HOST = '0.0.0.0' # accept any IP-adres
PORT = 2000 # Port to listen on
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
        
    conn, addr = s.accept()

#Aandrijving textboxes
txt_max_current = None
txt_max_tension = None

#Belasting textboxes
txt_max_watt = None
txt_max_speed = None
txt_max_temperature = None

#setpoint textboxes
txt_watt_setpoint = None
txt_speed_setpoint = None

#this is also a setpoint textbox, but is set apart from the remaining, and is common to both manual and automatic modes
txt_temperature_setpoint = None

#constant elements ------------------------------------------------------------------------
TOP_MESSAGE = "Welkom bij de MCU van groep 1 vul alle oderstaande gegevens in."
FINAL_MESSAGE = "Alle waarde zijn succesvol ontvangen systeem initializatie wordt gestart!"
MANUAL_RADIOBUTTON_TEXT = "Manueel"
AUTOMATIC_RADIOBUTTON_TEXT = "Profiel"
SUBMIT_MESSAGE = "Alle waarde zijn succesvol ontvangen systeem initializatie wordt gestart!"
MANUAL_SELECTION_MESSAGE = "Het systeem is manueel ingesteld u kunt de gewenste waarde invullen."
AUTOMATIC_SELECTION_MESSAGE = "U heeft een automatisch geselecteerd de waarde voor de setpoints worden automatisch invevult."
SAFETY_PARAMETERS_GROUP_HEADER_TEXT = "Veiligheidsparamaters"
SETPOINTS_GROUP_HEADER_TEXT = "Setpoints"
DRIVE_HEADER_TEXT = "Aandrijving"
RATE_HEADER_TEXT = "Belasting"
MAX_CURRENT_TEXT = "Max stroom"
MAX_TENSION_TEXT = "Max spanning"
MAX_WATT_TEXT = "Max vermogen"
MAX_SPEED_TEXT = "Max snelheid"
MAX_TEMPERATURE_TEXT = "Max tempratuur"
WATT_SETPOINT_TEXT = "Vermogen setpoint"
SPEED_SETPOINT_TEXT = "Snelheidsetpoint"
VOLT_LABEL_TEXT = "(Volt)"
AMPERE_LABEL_TEXT = "(Ampère)"
WATT_LABEL_TEXT = "(Watt)"
RPM_LABEL_TEXT = "(Rotaties per Minuut)"
DEGREES_LABEL_TEXT = "(ºC)"
TEMPERATURE_SETPOINT_TEXT = "Temperatuur setpoint"
FONT_SETTINGS_L = ("Arial", 12)
FONT_SETTINGS_M = ("Arial", 10)
FONT_SETTINGS_S = ("Arial", 9)
ROOT_WINDOW_WIDTH = 700
ROOT_WINDOW_HEIGHT = 440
MANUAL_SYSTEM_ID = 1
AUTOMATIC_SYSTEM_ID = 2
TOP_SEPARATOR_COLOR = "#22C4D3"
manueel = '1'
automatisch = '0'
EMPTY_STRING = ""
selectedId = 1

#make global variables ------------------------------------------------------------------------
#aandrijving
global max_stroom
global max_spanning
    
#belasting
global max_vermogen
global max_snelheid
global max_temperatuur
    
#setpoints
global max_vermogen_setpoint
global max_snelheid_setpoint
global temperatuur_zerina

#testwaarde voor automatische instellingen aanmaken ------------------------------------------------------------------------
#max vermogen setpoint
global max_vermogen_setpoint_1 
global max_vermogen_setpoint_2
global max_vermogen_setpoint_3
global max_vermogen_setpoint_4
global max_vermogen_setpoint_5
global max_vermogen_setpoint_6
global max_vermogen_setpoint_7
global max_vermogen_setpoint_8
global max_vermogen_setpoint_9
global max_vermogen_setpoint_10

#max snelheid setpoint
global max_snelheid_setpoint_1
global max_snelheid_setpoint_2
global max_snelheid_setpoint_3
global max_snelheid_setpoint_4
global max_snelheid_setpoint_5
global max_snelheid_setpoint_6
global max_snelheid_setpoint_7
global max_snelheid_setpoint_8
global max_snelheid_setpoint_9
global max_snelheid_setpoint_10

#testwaarde voor automatische instellingen vullen ------------------------------------------------------------------------
#max vermogen setpoint
max_vermogen_setpoint_1 = '2.5'
max_vermogen_setpoint_2 = '5'
max_vermogen_setpoint_3 = '7.5'
max_vermogen_setpoint_4 = '10'
max_vermogen_setpoint_5 = '12.5'
max_vermogen_setpoint_6 = '15'
max_vermogen_setpoint_7 = '17.5'
max_vermogen_setpoint_8 = '20'
max_vermogen_setpoint_9 = '22.5'
max_vermogen_setpoint_10 = '25'

#max snelheid setpoint
max_snelheid_setpoint_1 = '600'
max_snelheid_setpoint_2 = '1200'
max_snelheid_setpoint_3 = '1800'
max_snelheid_setpoint_4 = '2400'
max_snelheid_setpoint_5 = '3000'
max_snelheid_setpoint_6 = '3600'
max_snelheid_setpoint_7 = '4200'
max_snelheid_setpoint_8 = '4800'
max_snelheid_setpoint_9 = '5400'
max_snelheid_setpoint_10 = '6000'

#functions
def update_textboxes_state(selectedId):
    #profile
    state = 'normal' if (selectedId == MANUAL_SYSTEM_ID) else 'disabled'
    txt_speed_setpoint.configure(state=state)
    txt_watt_setpoint.configure(state=state)

    
def on_system_mode_change():
    global selectedId
    selectedId = selected.get()
    print("Selection: ",selectedId)
    update_textboxes_state(selectedId)
    
def make_group_header(master, text, padx):
    frm = Frame(master)
    frm.pack(fill=X)
    lbl_frm = Frame(frm)
    lbl_frm.pack(fill=X)
    lbl = Label(lbl_frm, text=text, font=FONT_SETTINGS_L)
    lbl.pack(side=LEFT)
    line = Frame(frm, bg=TOP_SEPARATOR_COLOR, height=1)
    line.pack(fill=X, padx=padx)
    
def make_content_header(master, text):
    frm = Frame(master)
    frm.pack(fill=X, pady=5)
    lbl = Label(frm, text=text, font=FONT_SETTINGS_M)
    lbl.pack(side=LEFT)
    
def make_labeled_textbox(master, left_text, right_text):
    frm = Frame(master)
    frm.pack(fill=X, padx=5)
    lbl = Label(frm, text=left_text, font=FONT_SETTINGS_M)
    lbl.pack(side=LEFT)
    txt = Entry(frm, width=10)
    txt.pack(side=RIGHT, padx=10)
    if (right_text != EMPTY_STRING):
        lbl = Label(frm, text=right_text, font=FONT_SETTINGS_S)
        lbl.pack(side=LEFT)
    return txt


def submit_button():
    global selected_profile
    #id bits
    id_bit_stroom = 'J'
    id_bit_spanning = 'B'
    id_bit_vermogen = 'P'
    id_bit_snelheid = 'V'
    id_bit_tempratuur = 'A'
    id_bit_vermogen_setpoint = 'W'
    id_bit_snelheid_setpoint = 'S'
   
    #read out textboxes
    if ( 0 <= int(txt_max_current.get()) <= 10):
        max_stroom = (txt_max_current.get())
    else:
        messagebox.showinfo("foutieve waarde!", "U heeft een foutieve waarde ingevult voor de Max stroom. Vul een getal tussen de 0 en 10 Ampère in en druk opnieuw op submit om door te gaan!")
        print(txt_max_current.get())
        return 0
    if 0 <= int(txt_max_tension.get()) <= 16:
        max_spanning = (txt_max_tension.get())
    else:
        messagebox.showinfo("foutieve waarde!", "U heeft een foutieve waarde ingevult voor de Max spanning. Vul een getal tussen de 0 en 16 Volt in en druk opnieuw op submit om door te gaan!")
        return 0
    if 0 <= int(txt_max_watt.get()) <= 25:
        max_vermogen = (txt_max_watt.get())
    else:
        messagebox.showinfo("foutieve waarde!", "U heeft een foutieve waarde ingevult voor de Max stroom. Vul een getal tussen de 0 en 25 Watt in en druk opnieuw op submit om door te gaan!")
        return 0
    if 0 <= int(txt_max_speed.get()) <= 6000:
        max_snelheid = (txt_max_speed.get())
    else:
        messagebox.showinfo("foutieve waarde!", "U heeft een foutieve waarde ingevult voor de Max speed. Vul een getal tussen de 0 en 6000 Rotaties per Minuut in en druk opnieuw op submit om door te gaan!")
        return 0
    if 0 <= int(txt_max_temperature.get()) <= 250:
        max_temperatuur = (txt_max_temperature.get())
    else:
        messagebox.showinfo("foutieve waarde!", "U heeft een foutieve waarde ingevult voor de Max temperatuur. Vul een getal tussen de 0 en 250 graden in en druk opnieuw op submit om door te gaan!")
        return 0
    if 0 <= int(txt_temperature_setpoint.get()) <= 25:
        temperatuur_zerina = (txt_temperature_setpoint.get())
    else:
        messagebox.showinfo("foutieve waarde!", "U heeft een foutieve waarde ingevult voor de Temperatuur setpoint. Vul een getal tussen de 0 en 25 Watt in en druk opnieuw op submit om door te gaan!")
        return 0


    
    #make send buffer and fill it with (decoded values)
    send_buffer = []
    #systeem is manueel ingesteld
    if selectedId == 1:
        if 0 <= int(txt_watt_setpoint.get()) <= 25:
            max_vermogen_setpoint = (txt_watt_setpoint.get())
        else:
            messagebox.showinfo("foutieve waarde!", "U heeft een foutieve waarde ingevult voor de watt setpoint. Vul een getal tussen de 0 en 25 watt in en druk opnieuw op submit om door te gaan!")
            return 0
        if 0 <= int(txt_speed_setpoint.get()) <= 6000:
            max_snelheid_setpoint = (txt_speed_setpoint.get())
        else:
            messagebox.showinfo("foutieve waarde!", "U heeft een foutieve waarde ingevult voor de snelheid setpoint. Vul een getal tussen de 0 en 6000 Rotaties per Minuut in en druk opnieuw op submit om door te gaan!")
            return 0
        send_buffer.append(manueel.encode('Cp1252'))
        send_buffer.append(id_bit_stroom.encode('Cp1252') + max_stroom.encode('Cp1252'))
        send_buffer.append(id_bit_spanning.encode('Cp1252') + max_spanning.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen.encode('Cp1252') + max_vermogen.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid.encode('Cp1252') + max_snelheid.encode('Cp1252'))
        send_buffer.append(id_bit_tempratuur.encode('Cp1252') + max_temperatuur.encode('Cp1252'))
        send_buffer.append(temperatuur_zerina.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint.encode('Cp1252'))
    #systeem is automatisch ingesteld
    else:
        #verstuur normale waarde
        send_buffer.append(automatisch.encode('Cp1252'))
        send_buffer.append(id_bit_stroom.encode('Cp1252') + max_stroom.encode('Cp1252'))
        send_buffer.append(id_bit_spanning.encode('Cp1252') + max_spanning.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen.encode('Cp1252') + max_vermogen.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid.encode('Cp1252') + max_snelheid.encode('Cp1252'))
        send_buffer.append(id_bit_tempratuur.encode('Cp1252') + max_temperatuur.encode('Cp1252'))
        #verstuur vermogen setpoints
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_1.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_2.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_3.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_4.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_5.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_6.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_7.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_8.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_9.encode('Cp1252'))
        send_buffer.append(id_bit_vermogen_setpoint.encode('Cp1252') + max_vermogen_setpoint_10.encode('Cp1252'))
        #stuur snelheid setpoints
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_1.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_2.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_3.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_4.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_5.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_6.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_7.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_8.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_9.encode('Cp1252'))
        send_buffer.append(id_bit_snelheid_setpoint.encode('Cp1252') + max_snelheid_setpoint_10.encode('Cp1252'))

        

    #display final message
    final_message.configure(text= FINAL_MESSAGE)
    
    #for i in range(0, len(max_stroom)):
    #    sendbuffer.append(max_stroom[i].encode('Cp1252'))
    
    #socket connection

    print(len(send_buffer))
    data = None
    decoded = None
    
    with conn:
        print('Connected by', addr)
        
        for x in send_buffer:
            conn.send(x)
            print('just sent')
            # TODO: uncomment the code below when the microcontroller code is sending any kind of message back to this code
            data = conn.recv(len(x))
            print(data)
            #decoded = data.decode('Cp1252')
            #print('just printing something: {%d}\n' % data)

#make window
window = Tk()
window.title("user settings")

#resize window (fit to screen)
width, height = window.winfo_screenwidth(), window.winfo_screenheight()
# window.geometry('%dx%d+0+0' % (width,height))
window.geometry("%dx%d+%d+%d" % (ROOT_WINDOW_WIDTH, ROOT_WINDOW_HEIGHT, ((width / 2) - (ROOT_WINDOW_WIDTH / 2)), ((height / 2) - (ROOT_WINDOW_HEIGHT / 2))))

#main frame for the window content
main_frame = Frame(window)
main_frame.pack(fill=BOTH, expand=True)

#make top message layout
frame = Frame(main_frame)
frame.pack(fill=X)
lbl = Label(frame, text=TOP_MESSAGE, font=FONT_SETTINGS_L)
lbl.pack(fill=X, pady=10)

#make mode selection radiobuttons
selected = IntVar()
selected.set(MANUAL_SYSTEM_ID)
frame = Frame(main_frame, bd=1, bg=TOP_SEPARATOR_COLOR)
frame.pack(fill=X, pady=20)
c_frame = Frame(height=25)
c_frame.place(in_=frame, anchor="c", relx=.5)
rad = Radiobutton(c_frame, text=MANUAL_RADIOBUTTON_TEXT, value=MANUAL_SYSTEM_ID, font=FONT_SETTINGS_M, variable=selected, command=on_system_mode_change)
# rad.pack(side=LEFT)
rad.pack()
rad = Radiobutton(c_frame, text=AUTOMATIC_RADIOBUTTON_TEXT, value=AUTOMATIC_SYSTEM_ID, font=FONT_SETTINGS_M, variable=selected, command=on_system_mode_change)
rad.pack()

#make the main content frame for both the safety parameter and setpoint groups
frame = Frame(main_frame)
frame.pack(fill=X, expand=True, padx=20, pady=10)

#make safety parameter inputs ------------------------------------------------------------------------
#make the group frame
g_frame = Frame(frame)
g_frame.pack(side=LEFT)

make_group_header(g_frame, SAFETY_PARAMETERS_GROUP_HEADER_TEXT, (0, 20))

#make Aandrijving manual safety parameters inputs
c_frame_inner = Frame(g_frame)
c_frame_inner.pack(fill=X, padx=20, pady=15)
make_content_header(c_frame_inner, DRIVE_HEADER_TEXT)
txt_max_current = make_labeled_textbox(c_frame_inner, MAX_CURRENT_TEXT, AMPERE_LABEL_TEXT)
txt_max_tension = make_labeled_textbox(c_frame_inner, MAX_TENSION_TEXT, VOLT_LABEL_TEXT)

#make Belasting manual safety parameters inputs
c_frame_inner = Frame(g_frame)
c_frame_inner.pack(fill=X, padx=20, pady=15)
make_content_header(c_frame_inner, RATE_HEADER_TEXT)
txt_max_watt = make_labeled_textbox(c_frame_inner, MAX_WATT_TEXT, WATT_LABEL_TEXT)
txt_max_speed = make_labeled_textbox(c_frame_inner, MAX_SPEED_TEXT, RPM_LABEL_TEXT)
txt_max_temperature = make_labeled_textbox(c_frame_inner, MAX_TEMPERATURE_TEXT, DEGREES_LABEL_TEXT)

#make setpoint inputs ------------------------------------------------------------------------
#make the group frame
g_frame = Frame(frame)
g_frame.pack(side=RIGHT)

make_group_header(g_frame, SETPOINTS_GROUP_HEADER_TEXT, (0, 0))

#make Aandrijving manual safety parameters inputs
c_frame_inner = Frame(g_frame)
c_frame_inner.pack(fill=X, pady=20)
txt_watt_setpoint = make_labeled_textbox(c_frame_inner, WATT_SETPOINT_TEXT, WATT_LABEL_TEXT)
txt_speed_setpoint = make_labeled_textbox(c_frame_inner, SPEED_SETPOINT_TEXT, RPM_LABEL_TEXT)

#make temperature setpoint element
c_frame_inner = Frame(g_frame)
c_frame_inner.pack(fill=X, pady=15)
txt_temperature_setpoint = make_labeled_textbox(c_frame_inner, TEMPERATURE_SETPOINT_TEXT, WATT_LABEL_TEXT)




#make submit button and final message text place
frame = Frame(main_frame)
frame.pack(fill=X, padx=20, pady=20)
submit_button = Button(frame, text="Submit", bg="#22C4D3", fg="white", font=FONT_SETTINGS_M, width=10,command= submit_button)
submit_button.pack(side=RIGHT)
final_message = Label(frame, text = EMPTY_STRING)
final_message.pack(side=LEFT)

window.mainloop()