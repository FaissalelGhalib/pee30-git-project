#import library
from tkinter import *
import socket
import time

HOST = '0.0.0.0' # accept any IP-adres
PORT = 2000 # Port to listen on
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
        
    conn, addr = s.accept()

#Aandrijving textboxes
txt_max_current = None
txt_max_tension = None

#Belasting textboxes
txt_max_watt = None
txt_max_speed = None
txt_max_temperature = None

#setpoint textboxes
txt_watt_setpoint = None
txt_speed_setpoint = None

#this is also a setpoint textbox, but is set apart from the remaining, and is common to both manual and automatic modes
txt_temperature_setpoint = None

#constant elements
TOP_MESSAGE = "Welkom bij de MCU van groep 1 vul alle oderstaande gegevens in."
FINAL_MESSAGE = "Alle waarde zijn succesvol ontvangen systeem initializatie wordt gestart!"
MANUAL_RADIOBUTTON_TEXT = "Manueel"
AUTOMATIC_RADIOBUTTON_TEXT = "Profiel"
SUBMIT_MESSAGE = "Alle waarde zijn succesvol ontvangen systeem initializatie wordt gestart!"
MANUAL_SELECTION_MESSAGE = "Het systeem is manueel ingesteld u kunt de gewenste waarde invullen."
AUTOMATIC_SELECTION_MESSAGE = "U heeft een automatisch geselecteerd de waarde voor de setpoints worden automatisch invevult."
SAFETY_PARAMETERS_GROUP_HEADER_TEXT = "Veiligheidsparamaters"
SETPOINTS_GROUP_HEADER_TEXT = "Setpoints"
DRIVE_HEADER_TEXT = "Aandrijving"
RATE_HEADER_TEXT = "Belasting"
MAX_CURRENT_TEXT = "Max stroom"
MAX_TENSION_TEXT = "Max spanning"
MAX_WATT_TEXT = "Max vermogen"
MAX_SPEED_TEXT = "Max snelheid"
MAX_TEMPERATURE_TEXT = "Max tempratuur"
WATT_SETPOINT_TEXT = "Vermogen setpoint"
SPEED_SETPOINT_TEXT = "Snelheidsetpoint"
VOLT_LABEL_TEXT = "(Volt)"
AMPERE_LABEL_TEXT = "(Ampère)"
WATT_LABEL_TEXT = "(Watt)"
RPM_LABEL_TEXT = "(Rotaties per Minuut)"
DEGREES_LABEL_TEXT = "(ºC)"
TEMPERATURE_SETPOINT_TEXT = "Temperatuur setpoint"
FONT_SETTINGS_L = ("Arial", 12)
FONT_SETTINGS_M = ("Arial", 10)
FONT_SETTINGS_S = ("Arial", 9)
ROOT_WINDOW_WIDTH = 700
ROOT_WINDOW_HEIGHT = 440
MANUAL_SYSTEM_ID = 1
AUTOMATIC_SYSTEM_ID = 2
TOP_SEPARATOR_COLOR = "#22C4D3"
EMPTY_STRING = ""

#functions
def update_textboxes_state(selectedId):
    state = 'normal' if (selectedId == MANUAL_SYSTEM_ID) else 'disabled'
    txt_speed_setpoint.configure(state=state)
    txt_watt_setpoint.configure(state=state)

    
def on_system_mode_change():
    selectedId = selected.get()
    print("Selection: ",selectedId)
    update_textboxes_state(selectedId)
    
def make_group_header(master, text, padx):
    frm = Frame(master)
    frm.pack(fill=X)
    lbl_frm = Frame(frm)
    lbl_frm.pack(fill=X)
    lbl = Label(lbl_frm, text=text, font=FONT_SETTINGS_L)
    lbl.pack(side=LEFT)
    line = Frame(frm, bg=TOP_SEPARATOR_COLOR, height=1)
    line.pack(fill=X, padx=padx)
    
def make_content_header(master, text):
    frm = Frame(master)
    frm.pack(fill=X, pady=5)
    lbl = Label(frm, text=text, font=FONT_SETTINGS_M)
    lbl.pack(side=LEFT)
    
def make_labeled_textbox(master, left_text, right_text):
    frm = Frame(master)
    frm.pack(fill=X, padx=5)
    lbl = Label(frm, text=left_text, font=FONT_SETTINGS_M)
    lbl.pack(side=LEFT)
    txt = Entry(frm, width=10)
    txt.pack(side=RIGHT, padx=10)
    if (right_text != EMPTY_STRING):
        lbl = Label(frm, text=right_text, font=FONT_SETTINGS_S)
        lbl.pack(side=LEFT)
    return txt

# old submit button
def my_submit_button_function_without_lists():
    # this was python setting the variables with the encoded values to send to the microcontroller
    sendbuffer = idbit1.encode('Cp1252') + max_stroom.encode('Cp1252')
    sendbuffer2 = max_spanning.encode('Cp1252')
    sendbuffer3 = max_vermogen.encode('Cp1252')
    
    # this was python sending the values to the microcontroller, one by one
    with conn:
        print('Connected by', addr)
        conn.send(sendbuffer)
        #time.sleep(0.5)
        conn.send(sendbuffer2)
        #time.sleep(0.5)
        
        #while(1):
        #    k = 1
            
        conn.send(sendbuffer3)

        while(1):
            k =1
            data = conn.recv(100)
            updatedata = data.decode('Cp1252')
            print(updatedata)

def submit_button():
    #fill variables to send to the micronctonroller
    #aandrijving
    global max_stroom
    global max_spanning
    
    #belasting
    global max_vermogen
    global max_snelheid
    global max_temperatuur
    
    #setpoints
    global max_vermogen_setpoint
    global max_snelheid_setpoint
    global temperatuur_zerina

    max_stroom = (txt_max_current.get())
    max_spanning = (txt_max_tension.get())
    max_vermogen = (txt_max_watt.get())
    max_snelheid = (txt_max_speed.get())
    maxtemperatuur = (txt_max_temperature.get())
    max_vermogen_setpoint = (txt_watt_setpoint.get())
    max_snelheid_setpoint = (txt_speed_setpoint.get())
    temperatuur_zerina = (txt_temperature_setpoint.get())
    print(max_vermogen_setpoint)
    print(max_snelheid)
    idbit1 = 'g'
    send_buffer = []
    send_buffer.append(idbit1.encode('Cp1252') + max_stroom.encode('Cp1252'))
    send_buffer.append(max_spanning.encode('Cp1252'))
    send_buffer.append(max_vermogen.encode('Cp1252'))

    #display final message
    final_message.configure(text= FINAL_MESSAGE)
    
    #for i in range(0, len(max_stroom)):
    #    sendbuffer.append(max_stroom[i].encode('Cp1252'))
    
    #socket connection

    print(len(send_buffer))
    data = None
    decoded = None
    
    with conn:
        print('Connected by', addr)
        
        for x in send_buffer:
            conn.send(x)
            print('just sent')
            # TODO: uncomment the code below when the microcontroller code is sending any kind of message back to this code
            data = conn.recv(len(x))
            print(data)
            #decoded = data.decode('Cp1252')
            #print('just printing something: {%d}\n' % data)

#make window
window = Tk()
window.title("user settings")

#resize window (fit to screen)
width, height = window.winfo_screenwidth(), window.winfo_screenheight()
# window.geometry('%dx%d+0+0' % (width,height))
window.geometry("%dx%d+%d+%d" % (ROOT_WINDOW_WIDTH, ROOT_WINDOW_HEIGHT, ((width / 2) - (ROOT_WINDOW_WIDTH / 2)), ((height / 2) - (ROOT_WINDOW_HEIGHT / 2))))

#main frame for the window content
main_frame = Frame(window)
main_frame.pack(fill=BOTH, expand=True)

#make top message layout
frame = Frame(main_frame)
frame.pack(fill=X)
lbl = Label(frame, text=TOP_MESSAGE, font=FONT_SETTINGS_L)
lbl.pack(fill=X, pady=10)

#make mode selection radiobuttons
selected = IntVar()
selected.set(MANUAL_SYSTEM_ID)
frame = Frame(main_frame, bd=1, bg=TOP_SEPARATOR_COLOR)
frame.pack(fill=X, pady=20)
c_frame = Frame(height=25)
c_frame.place(in_=frame, anchor="c", relx=.5)
rad = Radiobutton(c_frame, text=MANUAL_RADIOBUTTON_TEXT, value=MANUAL_SYSTEM_ID, font=FONT_SETTINGS_M, variable=selected, command=on_system_mode_change)
# rad.pack(side=LEFT)
rad.pack()
rad = Radiobutton(c_frame, text=AUTOMATIC_RADIOBUTTON_TEXT, value=AUTOMATIC_SYSTEM_ID, font=FONT_SETTINGS_M, variable=selected, command=on_system_mode_change)
rad.pack()

#make the main content frame for both the safety parameter and setpoint groups
frame = Frame(main_frame)
frame.pack(fill=X, expand=True, padx=20, pady=10)

#make safety parameter inputs ------------------------------------------------------------------------
#make the group frame
g_frame = Frame(frame)
g_frame.pack(side=LEFT)

make_group_header(g_frame, SAFETY_PARAMETERS_GROUP_HEADER_TEXT, (0, 20))

#make Aandrijving manual safety parameters inputs
c_frame_inner = Frame(g_frame)
c_frame_inner.pack(fill=X, padx=20, pady=15)
make_content_header(c_frame_inner, DRIVE_HEADER_TEXT)
txt_max_current = make_labeled_textbox(c_frame_inner, MAX_CURRENT_TEXT, AMPERE_LABEL_TEXT)
txt_max_tension = make_labeled_textbox(c_frame_inner, MAX_TENSION_TEXT, VOLT_LABEL_TEXT)

#make Belasting manual safety parameters inputs
c_frame_inner = Frame(g_frame)
c_frame_inner.pack(fill=X, padx=20, pady=15)
make_content_header(c_frame_inner, RATE_HEADER_TEXT)
txt_max_watt = make_labeled_textbox(c_frame_inner, MAX_WATT_TEXT, WATT_LABEL_TEXT)
txt_max_speed = make_labeled_textbox(c_frame_inner, MAX_SPEED_TEXT, RPM_LABEL_TEXT)
txt_max_temperature = make_labeled_textbox(c_frame_inner, MAX_TEMPERATURE_TEXT, DEGREES_LABEL_TEXT)

#make setpoint inputs ------------------------------------------------------------------------
#make the group frame
g_frame = Frame(frame)
g_frame.pack(side=RIGHT)

make_group_header(g_frame, SETPOINTS_GROUP_HEADER_TEXT, (0, 0))

#make Aandrijving manual safety parameters inputs
c_frame_inner = Frame(g_frame)
c_frame_inner.pack(fill=X, pady=20)
txt_watt_setpoint = make_labeled_textbox(c_frame_inner, WATT_SETPOINT_TEXT, WATT_LABEL_TEXT)
txt_speed_setpoint = make_labeled_textbox(c_frame_inner, SPEED_SETPOINT_TEXT, RPM_LABEL_TEXT)

#make temperature setpoint element
c_frame_inner = Frame(g_frame)
c_frame_inner.pack(fill=X, pady=15)
txt_temperature_setpoint = make_labeled_textbox(c_frame_inner, TEMPERATURE_SETPOINT_TEXT, WATT_LABEL_TEXT)




#make submit button and final message text place
frame = Frame(main_frame)
frame.pack(fill=X, padx=20, pady=20)
submit_button = Button(frame, text="Submit", bg="#22C4D3", fg="white", font=FONT_SETTINGS_M, width=10,command= submit_button)
submit_button.pack(side=RIGHT)
final_message = Label(frame, text = EMPTY_STRING)
final_message.pack(side=LEFT)

window.mainloop()