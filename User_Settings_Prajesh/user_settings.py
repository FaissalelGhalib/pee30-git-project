#import library
from tkinter import *
#functions output buttons
def submit_button():
    #lbl.configure(text="Alle waarde zijn succesvol ontvangen systeem initializatie wordt gestart!" + txt.get())
    print(selected.get)
def button_profiel():
    lbl.configure(text="het systeem is nu automatisch ingesteld")
#variable
Spanning = 'Volt'
Stroomsterkte = 'Ampère'
Vermogen = 'Watt'
Snelheid = 'Rotaties per Minuut'
#make window
window = Tk()
window.title("user settings")
#resize window (fit to screen)
width, height = window.winfo_screenwidth(), window.winfo_screenheight()
window.geometry('%dx%d+0+0' % (width,height))
#write text in the screen.
#profiel manueel
lbl = Label(window, text="Welkom bij de MCU van groep 1 hoe wilt u het systeem gebruiken?",)
lbl.grid(column=0, row=0)
#stroom (Ampère)
lbl = Label(window,text="Geef een waarde voor de maximale stroom.",)
lbl.grid(column=0, row=1, sticky=W)
lbl = Label(window,text= Stroomsterkte,)
lbl.grid(column=3, row=1, sticky=W)
#spanning (Volt)
lbl = Label(window, text="Geef een waarde voor de maximale spanning.",)
lbl.grid(column=0, row=2, sticky=W)
lbl = Label(window,text= Spanning,)
lbl.grid(column=3, row=2, sticky=W)
#vermogen setpoint
lbl = Label(window, text="Geef een waarde voor het vermogen setpoint.",)
lbl.grid(column=0, row=3, sticky=W)
lbl = Label(window,text= Vermogen,)
lbl.grid(column=3, row=3, sticky=W)
#maximale vermogen
lbl = Label(window, text="Geef een waarde voor de maximale vermogen van de belasting.",)
lbl.grid(column=0, row=4,sticky=W)
lbl = Label(window,text= Vermogen,)
lbl.grid(column=3, row=4, sticky=W)
#snelheidssetpoint
lbl = Label(window, text="Geef een waarde voor de snelheidssetpoint.",)
lbl.grid(column=0, row=5, sticky=W)
lbl = Label(window,text= Snelheid,)
lbl.grid(column=3, row=5, sticky=W)
#maximale snelheid
lbl = Label(window, text="Geef een waarde voor de maximale snelheid.",)
lbl.grid(column=0, row=6, sticky=W)
lbl = Label(window,text= Snelheid,)
lbl.grid(column=3, row=6, sticky=W)
#vermogen setpoint (tempratuur)
lbl = Label(window, text="Geef een waarde voor het vermogen setpoint(tempratuur).",)
lbl.grid(column=0, row=7,sticky=W)
lbl = Label(window,text= Vermogen,)
lbl.grid(column=3, row=7, sticky=W)
#vermogen (tempratuur)
lbl = Label(window, text="Geef een waarde voor het maximale vermogen (tempratuur).",)
lbl.grid(column=0, row=8,sticky=W)
lbl = Label(window,text= Vermogen,)
lbl.grid(column=3, row=8, sticky=W)
#radio buttons for selecting profiel/manueel
selected = IntVar()
rad1 = Radiobutton(window,text='manueel', value=1, variable= selected)
rad2 = Radiobutton(window,text='profiel vermogen setpoint', value=2, variable= selected)
rad3 = Radiobutton(window,text='profiel snelheid setpoint', value=3, variable= selected)
rad4 = Radiobutton(window,text='profiel vermogen setpoint (tempratuur)', value=4, variable= selected) 
rad1.grid(column=1, row=0)
rad2.grid(column=2, row=0)
rad3.grid(column=3, row=0)
rad4.grid(column=4, row=0)

if selected.get() == 1:
    txt = Entry(window, width =10,)
    txt.grid(column=2, row=1)
    txt = Entry(window, width =10)
    txt.grid(column=2, row=2)
    txt = Entry(window, width =10)
    txt.grid(column=2, row=3)
    txt = Entry(window, width =10)
    txt.grid(column=2, row=4)
    txt = Entry(window, width =10)
    txt.grid(column=2, row=5)
    txt = Entry(window, width =10)
    txt.grid(column=2, row=6)
    txt = Entry(window, width =10)
    txt.grid(column=2, row=7)
    txt = Entry(window, width =10)
    txt.grid(column=2, row=8)


else:
#input text boxes
    txt = Entry(window, width =10, state = 'disabled')
    txt.grid(column=2, row=1)
    txt = Entry(window, width =10, state = 'disabled')
    txt.grid(column=2, row=2)
    txt = Entry(window, width =10, state = 'disabled')
    txt.grid(column=2, row=3)
    txt = Entry(window, width =10, state = 'disabled')
    txt.grid(column=2, row=4)
    txt = Entry(window, width =10, state = 'disabled')
    txt.grid(column=2, row=5)
    txt = Entry(window, width =10, state = 'disabled')
    txt.grid(column=2, row=6)
    txt = Entry(window, width =10, state = 'disabled')
    txt.grid(column=2, row=7)
    txt = Entry(window, width =10, state = 'disabled')
    txt.grid(column=2, row=8)


#make buttons
btn = Button(window, text="submit", bg="green", fg="black" ,command= submit_button)
btn.grid(column=3, row=9)
window.mainloop()
 
##make buttons
#btn = Button(window, text="manueel", command = button_manueel)
#btn.grid(column=1, row=0)
#btn = Button(window, text="profiel", command = button_profiel)
#btn.grid(column=2, row=0)
##input textbox
#txt = Entry(window,width=10)
#txt.grid(column=0, row=1)
#txt = Entry(window,width=50)
#txt.grid(column=0, row=2)
##make sure the textbox is focussed
#txt.focus()
##repeat the window so it stays on the screen
window.mainloop()