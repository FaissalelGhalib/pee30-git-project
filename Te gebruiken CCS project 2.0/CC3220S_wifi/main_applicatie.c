// C header files
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
// Driver Header files
#include <ti/drivers/GPIO.h>
#include <ti/drivers/gpio/GPIOCC32XX.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>
// RTOS header files
#include <ti/sysbios/BIOS.h>
// POSIX Header files
#include <ti/posix/ccs/pthread.h>
#include <ti/posix/ccs/semaphore.h>

// Eigen headers
#include "uart_term.h"
#include "bestanden.h"

/* Example/Board Header files */
#include "Board.h"

/*PWM header file*/
#include <ti/drivers/PWM.h>
#include <mqueue.h>
#include <ti/net/mqtt/mqttclient.h>
//I2C driver
#include <ti/drivers/I2C.h>
#include <unistd.h>
// Taken
void *App_Task(void *args);

// Functies
void PingResultaat(SlNetAppPingReport_t *pReport);
int StelWifiIn(SlWlanSecParams_t *SecParams, SlWlanSecParamsExt_t *SecExtParams, _i8 *wachtwoord, bool vraagOmWachtwoord);
void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args));

// Semaphore voor taaksynchronisatie
sem_t wachtenOpIP;

// Wifi instellingen
#define IDENTITY ""//"12345678@hr.nl"
#define SSID "Egghead"

int startstatus = -1;

//GLOBALE postbussen
mqd_t ver_ele_to_userdis; //omgevingstemperatuur
mqd_t com_to_userdis; //communicatie naar userdisplay
mqd_t userinter_to_ver; //userinterfase-vermogen
mqd_t user_to_i2cCom; // prajesh naar I2c communicatie
mqd_t i2c_to_Userdis; //I2C_Communicatie naar UserDisplay, Faissal

/*Aanmaken van mcu, Aandrijving en Belasting adressen*/

#define mcuAdres         0x02;
#define AandrijvingAdres 0x04;
#define BelastingAdres   0x06;

//interrupt startknop
void start(uint_least8_t index)
{
    startstatus = 1;
    printf("%d\n", startstatus);

    //zet pin 21 hoog startline
    GPIO_write(Board_GPIO_LED4, Board_GPIO_LED_ON);

    GPIO_write(Board_GPIO_LED5, Board_GPIO_LED_OFF);
    // pin 7 rode led laag zetten
    //GPIO_write(Board_GPIO_LED3, Board_GPIO_LED_OFF);

}

//interrupt noodknop
void stop(uint_least8_t index)
{
    startstatus = 0;
    printf("%d\n", startstatus);
    GPIO_write(Board_GPIO_LED4, Board_GPIO_LED_OFF);
    GPIO_write(Board_GPIO_LED5, Board_GPIO_LED_ON);
}


void *Communicatie_binnen_de_MCU(void *pvParameters)
{

    unsigned int prio = 1;
    //char dummywaarde[5] = {'V','1','0','0', 0};
    char selected_profile[30] = {0};
    mq_receive(user_to_i2cCom, (char *)selected_profile, sizeof(selected_profile),&prio);

    if(selected_profile[0] == '1'){

        char max_stroom[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_stroom, sizeof(max_stroom),&prio);

        char max_spanning[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_spanning, sizeof(max_spanning),&prio);

        char max_vermogen[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen, sizeof(max_vermogen),&prio);

        char max_snelheid[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid, sizeof(max_snelheid),&prio);

        char max_temperatuur[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_temperatuur, sizeof(max_temperatuur),&prio);

        char max_vermogen_setpoint[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint, sizeof(max_temperatuur),&prio);

        char max_snelheid_setpoint[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint, sizeof(max_temperatuur),&prio);

        //maak INIT lijn hoog

        I2C_init(); //Initialiseer I2C

        I2C_Handle i2cHandle; //i2cHandle van het type I2C_Handle
        i2cHandle = I2C_open(0, NULL); // openen in default instelling

        /* Is de handle goed */
        if (i2cHandle == NULL)
        {
            // Error opening I2C
            while (1)
            {
            }
        }

        I2C_Transaction i2cTransaction = { 0 };

        uint8_t LeesAandrijving[4];

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_stroom;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        bool status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        i2cTransaction.writeBuf   = max_spanning;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        i2cTransaction.writeBuf   = max_vermogen;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        i2cTransaction.writeBuf   = max_snelheid;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        i2cTransaction.writeBuf   = max_temperatuur;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        i2cTransaction.writeBuf   = max_vermogen_setpoint;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        i2cTransaction.writeBuf   = max_snelheid_setpoint;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        //WACHT TOT BEIDE GEREED LIJNEN HOOG ZIJN:
        //---------------------------------------

        //---------------------------------------

        while(1){
            //Ontvang waardes nu en stuur ze door naar user display
        }
    }

    else{
        char max_stroom[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_stroom, sizeof(max_stroom),&prio);

        char max_spanning[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_spanning, sizeof(max_spanning),&prio);

        char max_vermogen[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen, sizeof(max_vermogen),&prio);

        char max_snelheid[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid, sizeof(max_snelheid),&prio);

        char max_temperatuur[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_temperatuur, sizeof(max_temperatuur),&prio);

        char max_vermogen_setpoint_1[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_1, sizeof(max_vermogen_setpoint_1),&prio);

        char max_vermogen_setpoint_2[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_2, sizeof(max_vermogen_setpoint_2),&prio);

        char max_vermogen_setpoint_3[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_3, sizeof(max_vermogen_setpoint_3),&prio);

        char max_vermogen_setpoint_4[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_4, sizeof(max_vermogen_setpoint_4),&prio);

        char max_vermogen_setpoint_5[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_5, sizeof(max_vermogen_setpoint_5),&prio);

        char max_vermogen_setpoint_6[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_6, sizeof(max_vermogen_setpoint_6),&prio);

        char max_vermogen_setpoint_7[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_7, sizeof(max_vermogen_setpoint_7),&prio);

        char max_vermogen_setpoint_8[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_8, sizeof(max_vermogen_setpoint_8),&prio);

        char max_vermogen_setpoint_9[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_9, sizeof(max_vermogen_setpoint_9),&prio);

        char max_vermogen_setpoint_10[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_vermogen_setpoint_10, sizeof(max_vermogen_setpoint_10),&prio);

        char max_snelheid_setpoint_1[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_1, sizeof(max_snelheid_setpoint_1),&prio);

        char max_snelheid_setpoint_2[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_2, sizeof(max_snelheid_setpoint_2),&prio);

        char max_snelheid_setpoint_3[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_3, sizeof(max_snelheid_setpoint_3),&prio);

        char max_snelheid_setpoint_4[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_4, sizeof(max_snelheid_setpoint_4),&prio);

        char max_snelheid_setpoint_5[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_5, sizeof(max_snelheid_setpoint_5),&prio);

        char  max_snelheid_setpoint_6[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_6, sizeof(max_snelheid_setpoint_6),&prio);

        char max_snelheid_setpoint_7[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_7, sizeof(max_snelheid_setpoint_7),&prio);

        char max_snelheid_setpoint_8[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_8, sizeof(max_snelheid_setpoint_8),&prio);

        char max_snelheid_setpoint_9[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_9, sizeof(max_snelheid_setpoint_9),&prio);

        char max_snelheid_setpoint_10[30] = {0};
        mq_receive(user_to_i2cCom, (char *)max_snelheid_setpoint_10, sizeof(max_snelheid_setpoint_10),&prio);

        printf("hallo2\n");


        I2C_init(); //Initialiseer I2C

        I2C_Handle i2cHandle; //i2cHandle van het type I2C_Handle
        i2cHandle = I2C_open(0, NULL); // openen in default instelling

        /* Is de handle goed */
        if (i2cHandle == NULL)
        {
            // Error opening I2C
            while (1)
            {
            }
        }

        I2C_Transaction i2cTransaction = { 0 };

        uint8_t LeesAandrijving[4];

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_stroom;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        bool status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_spanning;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_temperatuur;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        //WACHT OP BEIDE GEREEDLIJNEN
        //---------------------------

        //---------------------------

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_1;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_1;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_2;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_2;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_3;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_3;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_4;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_4;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_5;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_5;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_6;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_6;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_7;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_7;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_8;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_8;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_9;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_9;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        sleep(30);

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_vermogen_setpoint_10;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

        i2cTransaction.slaveAddress = AandrijvingAdres;

        /*schrijven  naar aandrijving*/

        i2cTransaction.writeBuf   = max_snelheid_setpoint_10;
        i2cTransaction.writeCount = 30;
        i2cTransaction.readBuf    = NULL;
        i2cTransaction.readCount  = 0;

       /*controle transactie, desnoods breakpoint voor testen*/

        status = I2C_transfer(i2cHandle, &i2cTransaction);

        if (status == false)
        {
            while(1);
        }

    }


   /* wacht 3 seconden*/
//    sleep(3);
//
//    /* lezen van de aandrijving, i2c slave aandrijving moet iets sturen*/
//    i2cTransaction.writeBuf = NULL;
//    i2cTransaction.writeCount = 0;
//    i2cTransaction.readBuf = LeesAandrijving;
//    i2cTransaction.readCount = 4;
//
//    status = I2C_transfer(i2cHandle, &i2cTransaction);
//
//    if (status == false)
//    {
//         while(1);
//    }

    /*Variabelen van aandrijving/belasting versturen naar Faissal*/

//             VermogenAandrijving[7] = 0;
//         unsigned int prio = 1;
//             mq_send(i2c_to_Userdis, (char *)temperatuur, 8,prio);

}

void *PWM(void *pvParameters)
{
    //PWM instellen

    PWM_Handle pwm;
    PWM_Params pwmParams;
    uint32_t   dutyValue;
    // Initialize the PWM driver.
    PWM_init();
    // Initialize the PWM parameters
    PWM_Params_init(&pwmParams);
    pwmParams.idleLevel = PWM_IDLE_LOW;      // Output low when PWM is not running
    pwmParams.periodUnits = PWM_PERIOD_HZ;   // Period is in Hz
    pwmParams.periodValue = 20e3 ;             //20kHz
    pwmParams.dutyUnits = PWM_DUTY_FRACTION; // Duty is in fractional percentage
    pwmParams.dutyValue = 0;                 // 0% initial duty cycle

    // Open the PWM instance in pin 1(GPIO_10)
    pwm = PWM_open(Board_PWM0, &pwmParams);
    if (pwm == NULL) {
        // PWM_open() failed
        while (1);
    }
    PWM_start(pwm);             // start PWM with 0% duty cycle

    unsigned int prio = 1;
    char vermogen[10];
    char newvermogen[10] = {0};
    mq_receive(userinter_to_ver,(char *)&vermogen,sizeof(vermogen),&prio);

    int i = 0;
    while(i != 9){
        newvermogen[i] = vermogen[i + 1];
        i++;
    }

    float ingevoerde_vermogen= atof(newvermogen);
    //duty cycle aanpassen, ingevoerde waarde moet tussen 0W en 25W liggen
    //float ingevoerde_vermogen = 15; //deze code moet nog gemaakt worden
    float y= ((1.3451 *ingevoerde_vermogen)+21.209);


    //int lengte_van_y= sizeof();
       char temperatuur[10] = {0};
       sprintf(temperatuur,"Z%f",y);
       temperatuur[9] = 0;

       mq_send(ver_ele_to_userdis, (char *)temperatuur, sizeof(temperatuur),prio);

       //dutycycle instellen
       dutyValue = (uint32_t) (((uint64_t) PWM_DUTY_FRACTION_MAX * (25-ingevoerde_vermogen)) / 25);
       PWM_setDuty(pwm, dutyValue); // set duty cycle

       return 0;

}

void *mainThread(void *arg0)
{
    //KNOPPPEN INSTELLEN

    /* Call driver init functions */
    GPIO_init();

    /* Configure the LED and button pin2*/
     GPIO_setConfig(Board_GPIO_BUTTON4, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
     //start start line pin 21
     GPIO_setConfig(Board_GPIO_LED4, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
     //pin 15
     GPIO_setConfig(Board_GPIO_LED5, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
     //op de uitgang pin 7
     GPIO_setConfig(Board_GPIO_LED3, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);



    /* install Button callback */
    GPIO_setCallback(Board_GPIO_BUTTON4, start);

    /* Enable interrupts */
    GPIO_enableInt(Board_GPIO_BUTTON4);

    if (Board_GPIO_BUTTON4 != Board_GPIO_BUTTON5)
    {
        GPIO_setConfig(Board_GPIO_BUTTON5, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING); //pin5
        /* Install Button callback */
        GPIO_setCallback(Board_GPIO_BUTTON5, stop);
        GPIO_enableInt(Board_GPIO_BUTTON5);
    }

    //GLOBALE POSTBUS AANMAKEN
    //postbus pwm-userdisplay
    struct mq_attr ver_ele_to_userdis_attr;
    ver_ele_to_userdis_attr.mq_maxmsg = 1;
    ver_ele_to_userdis_attr.mq_msgsize = 10;
    ver_ele_to_userdis_attr.mq_flags = 0;

    ver_ele_to_userdis = mq_open("ver_ele_to_userdis", O_RDWR | O_CREAT, 0, &ver_ele_to_userdis_attr);

    //postbus communicatie aandrijving belasting
    //de waardes hieronder moeten jullie aanpassen naar de gewenste waardes.
    struct mq_attr com_to_userdis_attr;
    com_to_userdis_attr.mq_maxmsg = 1;
    com_to_userdis_attr.mq_msgsize = 8;
    com_to_userdis_attr.mq_flags = 0;

    com_to_userdis = mq_open("com_to_userdis", O_RDWR | O_CREAT, 0, &com_to_userdis_attr);

    //postbus gebruiker naar PWM
    struct mq_attr userinter_to_ver_attr;
    userinter_to_ver_attr.mq_maxmsg = 1;
    userinter_to_ver_attr.mq_msgsize = 10;
    userinter_to_ver_attr.mq_flags = 0;

    userinter_to_ver = mq_open("userinter_to_ver", O_RDWR | O_CREAT, 0, &userinter_to_ver_attr);

    //Globale postbus User,Prajesh naar I2C_Communicatie

    struct mq_attr user_to_i2cCom_attr;

    user_to_i2cCom_attr.mq_maxmsg = 30;
    user_to_i2cCom_attr.mq_msgsize = 30;
    user_to_i2cCom_attr.mq_flags = 0;

    user_to_i2cCom = mq_open("user_to_i2cCom", O_RDWR | O_CREAT, 0, &user_to_i2cCom_attr);


    //Globale postbus I2C_Communicatie naar UserDisplay, Faissal

    struct mq_attr i2c_to_Userdis_attr;

    i2c_to_Userdis_attr.mq_maxmsg = 1;
    i2c_to_Userdis_attr.mq_msgsize = 100;
    i2c_to_Userdis_attr.mq_flags = 0;

    i2c_to_Userdis = mq_open("i2c_to_Userdis", O_RDWR | O_CREAT, 0, &i2c_to_Userdis_attr);

    // Variabelen
    int Status;
    SlWlanSecParams_t SecParams; // Wifi parameters
    SlWlanSecParamsExt_t SecExtParams; // Wifi enterprise parameters
    _i8 ww[64]; // Buffer om een wifi-wachtwoord in op te slaan

    // Intialisaties
    sem_init(&wachtenOpIP, 0, 0); // Semaphore voor synchronisatie
    SPI_init();
    InitTerm(); // UART functionaliteit van TI

    // Prio stack functie
    MaakTaak(9, 2048, sl_Task); // Nodig voor wifi api
    MaakTaak(2, 2048, App_Task); // Onze aplicatie
    MaakTaak(2, 2048, PWM);
    MaakTaak(2, 2048, Communicatie_binnen_de_MCU);

    // Hello
    printf("\nApplicatie gestart.\nOverige meldingen zijn te vinden op de UART terminal op 115200 baud.\n");
    UART_PRINT("Starten.\r\n");

    bool vraagOmWachtwoord = false; // Probeer eerst om opgeslagen wachtwoord te gebruiken
    do
    {
        // Wifi instellen en verbinden
        StelWifiIn(&SecParams, &SecExtParams, ww, vraagOmWachtwoord);

        Status = sl_WlanConnect((_i8*)SSID, // SSID
                                strlen(SSID), // Lengte van SSID
                                0,
                                &SecParams, // Security parameters wifi
                                0); // Extended security parameters enterprice wifi
        if (Status !=0)
        {
            UART_PRINT("Wifi instellingen fout: %d\r\n", Status);
            vraagOmWachtwoord = true; // Probeer nogmaals om een correct wachtwoord in te voeren
        }
    }


    while (Status != 0);

    /*!
     * De thread kan nu stoppen.
     * Wachten op succesvolle verbinding zodat de App-thread
     * verder kan gaan.
     *
     * @see: SimpleLinkNetAppEventHandler(),
     * @see: App_Task(void * args)
     *
     */
    return 0;
}


void *App_Task(void *args)
{
    SlSockAddrIn_t mijnAdres;

            _u16                    len = sizeof(SlNetCfgIpV4Args_t);
            _u16                    ConfigOpt = 0;
            SlNetCfgIpV4Args_t      ipV4 = {0}; //struct voor ip
            SlNetAppPingReport_t    report;     //struct voor ping
            SlNetAppPingCommand_t   pingCommand;//struct voor ping


            /* Wachten op een ip adres, daarna kunnen we verder.
             * zie ook SimpleLinkNetAppEventHandler() */
            sem_wait(&wachtenOpIP);

            // Wachtwoord alleen opslaan als verbinden gelukt is.
            schrijfWachtwoord();

            //haal ip op
            sl_NetCfgGet(SL_NETCFG_IPV4_STA_ADDR_MODE,
                         &ConfigOpt,
                         &len,
                         (_u8 *)&ipV4
                         );

            //pretty print ip
            UART_PRINT( "\r\n"
                        "Mijn IP\t %d.%d.%d.%d\r\n"
                        "MASK\t %d.%d.%d.%d\r\n"
                        "GW\t %d.%d.%d.%d\r\n"
                        "DNS\t %d.%d.%d.%d\r\n",
                SL_IPV4_BYTE(ipV4.Ip,3),SL_IPV4_BYTE(ipV4.Ip,2),SL_IPV4_BYTE(ipV4.Ip,1),SL_IPV4_BYTE(ipV4.Ip,0),
                SL_IPV4_BYTE(ipV4.IpMask,3),SL_IPV4_BYTE(ipV4.IpMask,2),SL_IPV4_BYTE(ipV4.IpMask,1),SL_IPV4_BYTE(ipV4.IpMask,0),
                SL_IPV4_BYTE(ipV4.IpGateway,3),SL_IPV4_BYTE(ipV4.IpGateway,2),SL_IPV4_BYTE(ipV4.IpGateway,1),SL_IPV4_BYTE(ipV4.IpGateway,0),
                SL_IPV4_BYTE(ipV4.IpDnsServer,3),SL_IPV4_BYTE(ipV4.IpDnsServer,2),SL_IPV4_BYTE(ipV4.IpDnsServer,1),SL_IPV4_BYTE(ipV4.IpDnsServer,0));

            _i16 Handle = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, SL_IPPROTO_TCP);

            if (Handle < 0){
                while(1);
            }

            mijnAdres.sin_port = sl_Htons(2000);
            mijnAdres.sin_addr.s_addr = sl_Htonl(SL_IPV4_VAL(192, 168, 43, 38));
            mijnAdres.sin_family = SL_AF_INET;

            _i16 connect = sl_Connect(Handle, (SlSockAddr_t*)&mijnAdres, sizeof(SlSockAddrIn_t));

            if(connect < 0){
                while(1);
            }
            _i8 buffer[100];

            SlTransceiverRxOverHead_t *transHeader;

            unsigned int prio = 1;



            // veiligheid parameters
            _i8 selected_profile[30] = {0};
            _i8 max_stroom[30] = {0};
            _i8 max_spanning[30] = {0};
            _i8 max_vermogen[30] = {0};
            _i8 max_snelheid[30] = {0};
            _i8 max_temperatuur[30] = {0};
            _i8 temperatuur_zerina[10] ={0};
            // setpoints (manueel)
            _i8 max_vermogen_setpoint[30] = {0};
            _i8 max_snelheid_setpoint[30] = {0};
            //setpoints (automatisch)
            _i8 max_vermogen_setpoint_1[30] = {0};
            _i8 max_vermogen_setpoint_2[30] = {0};
            _i8 max_vermogen_setpoint_3[30] = {0};
            _i8 max_vermogen_setpoint_4[30] = {0};
            _i8 max_vermogen_setpoint_5[30] = {0};
            _i8 max_vermogen_setpoint_6[30] = {0};
            _i8 max_vermogen_setpoint_7[30] = {0};
            _i8 max_vermogen_setpoint_8[30] = {0};
            _i8 max_vermogen_setpoint_9[30] = {0};
            _i8 max_vermogen_setpoint_10[30] = {0};

            _i8 max_snelheid_setpoint_1[30] = {0};
            _i8 max_snelheid_setpoint_2[30] = {0};
            _i8 max_snelheid_setpoint_3[30] = {0};
            _i8 max_snelheid_setpoint_4[30] = {0};
            _i8 max_snelheid_setpoint_5[30] = {0};
            _i8 max_snelheid_setpoint_6[30] = {0};
            _i8 max_snelheid_setpoint_7[30] = {0};
            _i8 max_snelheid_setpoint_8[30] = {0};
            _i8 max_snelheid_setpoint_9[30] = {0};
            _i8 max_snelheid_setpoint_10[30] = {0};


            char antwoord[] = "1";

            //mq_receive(ver_ele_to_userdis,(char *)&O_temp,sizeof(O_temp),&prio);
            //printf("%s",O_temp);

            //mq_receive(i2c_to_Userdis,(char *)&snelheid,sizeof(snelheid),&prio);
            //printf("%s\n", snelheid);

            //while(1){

            //while(1){
                // receive profile
                int i = sl_Recv(Handle, selected_profile, 24, 0);
                selected_profile[i] = 0;
                transHeader = (SlTransceiverRxOverHead_t*)selected_profile;
                UART_PRINT("ontvangen %s", selected_profile);
                printf("%s\n", selected_profile);
                //send an answer
                sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                if (selected_profile[0] == '1'){
                       i = sl_Recv(Handle, max_stroom, 24, 0);
                       transHeader = (SlTransceiverRxOverHead_t*)max_stroom;
                       UART_PRINT("ontvangen %s", max_stroom);
                       printf("%s\n", max_stroom);
                       //send an answer
                       sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                       i = sl_Recv(Handle, max_spanning, 24, 0);
                       transHeader = (SlTransceiverRxOverHead_t*)max_spanning;
                       UART_PRINT("ontvangen %s", max_spanning);
                       printf("%s\n", max_spanning);
                       //send an answer
                       sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                       i = sl_Recv(Handle, max_vermogen, 24, 0);
                       transHeader = (SlTransceiverRxOverHead_t*)max_vermogen;
                       UART_PRINT("ontvangen %s", max_vermogen);
                       printf("%s\n", max_vermogen);
                       //send an answer
                       sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                       i = sl_Recv(Handle, max_snelheid, 24, 0);
                       transHeader = (SlTransceiverRxOverHead_t*)max_snelheid;
                       UART_PRINT("ontvangen %s", max_snelheid);
                       printf("%s\n", max_snelheid);
                       //send an answer
                       sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                       i = sl_Recv(Handle, max_temperatuur, 24, 0);
                       transHeader = (SlTransceiverRxOverHead_t*)max_temperatuur;
                       UART_PRINT("ontvangen %s", max_temperatuur);
                       printf("%s\n", max_temperatuur);
                       //send an answer
                       sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                       i = sl_Recv(Handle, temperatuur_zerina, 24, 0);
                       transHeader = (SlTransceiverRxOverHead_t*)temperatuur_zerina;
                       UART_PRINT("ontvangen %s", temperatuur_zerina);
                       printf("%s\n", temperatuur_zerina);
                       //send an answer
                       sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                       i = sl_Recv(Handle, max_vermogen_setpoint, 24, 0);
                       transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint;
                       UART_PRINT("ontvangen %s", max_vermogen_setpoint);
                       printf("%s\n", max_vermogen_setpoint);
                       //send an answer
                       sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                       i = sl_Recv(Handle, max_snelheid_setpoint, 24, 0);
                       transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint;
                       UART_PRINT("ontvangen %s", max_snelheid_setpoint);
                       printf("%s\n", max_snelheid_setpoint);
                       //send an answer
                       sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                       mq_send(user_to_i2cCom, (char *)selected_profile, sizeof(selected_profile),prio);
                       mq_send(user_to_i2cCom, (char *)max_stroom, sizeof(max_stroom),prio);
                       mq_send(user_to_i2cCom, (char *)max_spanning, sizeof(max_spanning),prio);
                       mq_send(user_to_i2cCom, (char *)max_vermogen, sizeof(max_vermogen),prio);
                       mq_send(user_to_i2cCom, (char *)max_snelheid, sizeof(max_snelheid),prio);
                       mq_send(user_to_i2cCom, (char *)max_temperatuur, sizeof(max_temperatuur),prio);
                       mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint, sizeof(max_vermogen_setpoint),prio);
                       mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint, sizeof(max_snelheid_setpoint),prio);

                }

                // if the system selected a profile enter this code
                else{
                    i = sl_Recv(Handle, max_stroom, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_stroom;
                    UART_PRINT("ontvangen %s", max_stroom);
                    printf("%s\n", max_stroom);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_spanning, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_spanning;
                    UART_PRINT("ontvangen %s", max_spanning);
                    printf("%s\n", max_spanning);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_vermogen, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen;
                    UART_PRINT("ontvangen %s", max_vermogen);
                    printf("%s\n", max_vermogen);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid;
                    UART_PRINT("ontvangen %s", max_snelheid);
                    printf("%s\n", max_snelheid);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_temperatuur, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_temperatuur;
                    UART_PRINT("ontvangen %s", max_temperatuur);
                    printf("%s\n", max_temperatuur);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, temperatuur_zerina, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)temperatuur_zerina;
                    UART_PRINT("ontvangen %s", temperatuur_zerina);
                    printf("%s\n", temperatuur_zerina);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    //ontvang setpoints 10 testwaarde per setpoint
                    i = sl_Recv(Handle, max_vermogen_setpoint_1, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_1;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_1);
                    printf("%s\n", max_vermogen_setpoint_1);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    sleep(0.2);

                    i = sl_Recv(Handle, max_vermogen_setpoint_2, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_2;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_2);
                    printf("%s\n", max_vermogen_setpoint_2);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_vermogen_setpoint_3, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_3;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_3);
                    printf("%s\n", max_vermogen_setpoint_3);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);
                    sleep(0.2);
                    i = sl_Recv(Handle, max_vermogen_setpoint_4, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_4;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_4);
                    printf("%s\n", max_vermogen_setpoint_4);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_vermogen_setpoint_5, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_5;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_5);
                    printf("%s\n", max_vermogen_setpoint_5);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_vermogen_setpoint_6, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_6;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_6);
                    printf("%s\n", max_vermogen_setpoint_6);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_vermogen_setpoint_7, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_7;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_7);
                    printf("%s\n", max_vermogen_setpoint_7);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_vermogen_setpoint_8, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_8;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_8);
                    printf("%s\n", max_vermogen_setpoint_8);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_vermogen_setpoint_9, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_9;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_9);
                    printf("%s\n", max_vermogen_setpoint_9);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_vermogen_setpoint_10, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_vermogen_setpoint_10;
                    UART_PRINT("ontvangen %s", max_vermogen_setpoint_10);
                    printf("%s\n", max_vermogen_setpoint_10);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);




                    i = sl_Recv(Handle, max_snelheid_setpoint_1, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_1;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_1);
                    printf("%s\n", max_snelheid_setpoint_1);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_2, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_2;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_2);
                    printf("%s\n", max_snelheid_setpoint_2);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_3, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_3;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_3);
                    printf("%s\n", max_snelheid_setpoint_3);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_4, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_4;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_4);
                    printf("%s\n", max_snelheid_setpoint_4);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_5, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_5;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_5);
                    printf("%s\n", max_snelheid_setpoint_5);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_6, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_6;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_6);
                    printf("%s\n", max_snelheid_setpoint_6);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_7, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_7;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_7);
                    printf("%s\n", max_snelheid_setpoint_7);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_8, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_8;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_8);
                    printf("%s\n", max_snelheid_setpoint_8);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_9, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_9;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_9);
                    printf("%s\n", max_snelheid_setpoint_9);
                    //send an answer
                    sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    i = sl_Recv(Handle, max_snelheid_setpoint_10, 24, 0);
                    transHeader = (SlTransceiverRxOverHead_t*)max_snelheid_setpoint_10;
                    UART_PRINT("ontvangen %s", max_snelheid_setpoint_10);
                    printf("%s\n", max_snelheid_setpoint_10);
                    //send an answer
                    //sl_Send(Handle, antwoord, sizeof(antwoord), 0);

                    mq_send(user_to_i2cCom, (char *)selected_profile, sizeof(selected_profile),prio);
                    mq_send(user_to_i2cCom, (char *)max_stroom, sizeof(max_stroom),prio);
                    mq_send(user_to_i2cCom, (char *)max_spanning, sizeof(max_spanning),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen, sizeof(max_vermogen),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid, sizeof(max_snelheid),prio);
                    mq_send(user_to_i2cCom, (char *)max_temperatuur, sizeof(max_temperatuur),prio);

                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_1, sizeof(max_vermogen_setpoint_1),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_2, sizeof(max_vermogen_setpoint_2),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_3, sizeof(max_vermogen_setpoint_3),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_4, sizeof(max_vermogen_setpoint_4),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_5, sizeof(max_vermogen_setpoint_5),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_6, sizeof(max_vermogen_setpoint_6),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_7, sizeof(max_vermogen_setpoint_7),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_8, sizeof(max_vermogen_setpoint_8),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_9, sizeof(max_vermogen_setpoint_9),prio);
                    mq_send(user_to_i2cCom, (char *)max_vermogen_setpoint_10, sizeof(max_vermogen_setpoint_10),prio);

                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_1, sizeof(max_snelheid_setpoint_1),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_2, sizeof(max_snelheid_setpoint_2),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_3, sizeof(max_snelheid_setpoint_3),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_4, sizeof(max_snelheid_setpoint_4),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_5, sizeof(max_snelheid_setpoint_5),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_6, sizeof(max_snelheid_setpoint_6),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_7, sizeof(max_snelheid_setpoint_7),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_8, sizeof(max_snelheid_setpoint_8),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_9, sizeof(max_snelheid_setpoint_9),prio);
                    mq_send(user_to_i2cCom, (char *)max_snelheid_setpoint_10, sizeof(max_snelheid_setpoint_10),prio);
                }
                //sturen naar PWM thread

                mq_send(userinter_to_ver, (char *)temperatuur_zerina, sizeof(temperatuur_zerina),prio);

                sl_Close(Handle);

                Handle = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, SL_IPPROTO_TCP);

                if (Handle < 0){
                    while(1);
                }
                mijnAdres.sin_port = sl_Htons(2000);
                mijnAdres.sin_addr.s_addr = sl_Htonl(SL_IPV4_VAL(192, 168, 43, 152));
                mijnAdres.sin_family = SL_AF_INET;

                connect = sl_Connect(Handle, (SlSockAddr_t*)&mijnAdres, sizeof(SlSockAddrIn_t));

                if(connect < 0){
                    while(1);
                }

                //omgevingstemperatuur buffer
                char O_temp[10] = {0};
                char snelheid[10] = "V400";
                char vermogenbelasting[10] = "Q20";
                char rendementaandrijving[10] = "N40";
                char aandrijvingvermogen[10] = "X5";
                char rendementbelasting[10] = "M45";
                char omgevingstemperatuur[10] = "Z30";
                char belastingtemperatuur[10] = "A40";
                char belastingkoppel[10] = "K30";
               // mq_receive(ver_ele_to_userdis,(char *)&O_temp,sizeof(O_temp),&prio);

                while(1){

                    sl_Send(Handle, snelheid, sizeof(O_temp), NULL);
                    transHeader = (SlTransceiverRxOverHead_t*)buffer;
                    UART_PRINT("ontvangen %s", buffer); //[sizeof(SlTransceiverRxOverHead_t)]);

                    sl_Send(Handle, vermogenbelasting, sizeof(O_temp), NULL);
                    transHeader = (SlTransceiverRxOverHead_t*)buffer;
                    UART_PRINT("ontvangen %s", buffer); //[sizeof(SlTransceiverRxOverHead_t)]);

                    sl_Send(Handle, rendementaandrijving, sizeof(O_temp), NULL);
                    transHeader = (SlTransceiverRxOverHead_t*)buffer;
                    UART_PRINT("ontvangen %s", buffer); //[sizeof(SlTransceiverRxOverHead_t)]);

                    sl_Send(Handle, aandrijvingvermogen, sizeof(O_temp), NULL);
                    transHeader = (SlTransceiverRxOverHead_t*)buffer;
                    UART_PRINT("ontvangen %s", buffer); //[sizeof(SlTransceiverRxOverHead_t)]);

                    sl_Send(Handle, rendementbelasting, sizeof(O_temp), NULL);
                    transHeader = (SlTransceiverRxOverHead_t*)buffer;
                    UART_PRINT("ontvangen %s", buffer); //[sizeof(SlTransceiverRxOverHead_t)]);

                    sl_Send(Handle, omgevingstemperatuur, sizeof(O_temp), NULL);
                    transHeader = (SlTransceiverRxOverHead_t*)buffer;
                    UART_PRINT("ontvangen %s", buffer); //[sizeof(SlTransceiverRxOverHead_t)]);

                    sl_Send(Handle, belastingtemperatuur, sizeof(O_temp), NULL);
                    transHeader = (SlTransceiverRxOverHead_t*)buffer;
                    UART_PRINT("ontvangen %s", buffer); //[sizeof(SlTransceiverRxOverHead_t)]);

                    sl_Send(Handle, belastingkoppel, sizeof(O_temp), NULL);
                    transHeader = (SlTransceiverRxOverHead_t*)buffer;
                    UART_PRINT("ontvangen %s", buffer); //[sizeof(SlTransceiverRxOverHead_t)]);
                    sleep(1);
                }


            //}
}

// Callback van ping functie
void PingResultaat(SlNetAppPingReport_t* pReport)
{
    // Resultaten afdrukken.
    UART_PRINT("Verzonden: %d, "
               "Ontvangen: %d\r\n"
               "Gemiddelde Responsietijd: %dms\r\n",
               pReport->PacketsSent,
               pReport->PacketsReceived,
               pReport->AvgRoundTime);

    // Klaar met ping applicatie
}

void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args))
{
    pthread_t Thread;
    pthread_attr_t attrs;
    struct sched_param priParam;
    int retc;

    // sl_task thread maken. Dit handelt alle callbacks van wifi af
    priParam.sched_priority = prioriteit;
    retc = pthread_attr_init(&attrs);
    retc |= pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED); // Ontkoppelde taak
    retc |= pthread_attr_setschedparam(&attrs, &priParam); // Prioriteit instellen
    retc |= pthread_attr_setstacksize(&attrs, stackSize); // Stacksize instellen

    retc |= pthread_create(&Thread, &attrs, functie, NULL); // Taak starten
    if (retc != 0)
    {
        // pthread_create() failed
        UART_PRINT("Taak fout: %d",retc);
        while (1);
    }
}

int StelWifiIn(SlWlanSecParams_t* SecParams, SlWlanSecParamsExt_t* SecExtParams, _i8* wachtwoord, bool vraagOmWachtwoord)
{
    int Status;
    SlDateTime_t dateTime = {0};
    _u8 serverAuthenticatie = 0;

    // NWP starten en configureren
    sl_Start(0, 0, 0);
    Status = sl_WlanSetMode(ROLE_STA); // Station modus

    /* Om te voorkomen dat jouw wachtwoord in je code is te vinden wordt het
     * geschreven naar een bestand op de flash.
     *
     * Je moet eenmalig het wachtwoord geven, elke keer daarna kun je het
     * alleen uitlezen. Als je het wilt aanpassen dan moet parameter vraagOmWachtwoord
     * true worden.
     * */
    leesWachtwoord((char *)wachtwoord, vraagOmWachtwoord);

    SecParams->Key = wachtwoord;
    SecParams->KeyLen = strlen((const char*)wachtwoord);
    SecParams->Type = SL_WLAN_SEC_TYPE_WPA_WPA2;

    // extended alleen nodig voor enterprise wifi
    SecExtParams->User = IDENTITY;
    SecExtParams->UserLen = strlen(IDENTITY);
    SecExtParams->AnonUser = 0;
    SecExtParams->AnonUserLen = 0;
    SecExtParams->EapMethod = SL_WLAN_ENT_EAP_METHOD_PEAP0_MSCHAPv2;

    // Jaar instellen zodat het certificaat niet wordt afgekeurd
    dateTime.tm_year =  (_u32)2019; // Year (YYYY format)

    // NWP herstarten om STA modus ook te gebruiken
    sl_Stop(0);
    sl_Start(0, 0, 0);

    // Tijd opslaan
    sl_DeviceSet(SL_DEVICE_GENERAL,
                 SL_DEVICE_GENERAL_DATE_TIME,
                 sizeof(SlDateTime_t),
                 (_u8 *)(&dateTime));

    // Eduroam geeft geen CA certificaat uit, dus check uitzetten
    Status = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, SL_WLAN_GENERAL_PARAM_DISABLE_ENT_SERVER_AUTH, 1, &serverAuthenticatie);

    return Status;
}
