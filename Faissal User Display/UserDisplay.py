#Importeer alle benodigde libraries.
import socket
import random
from itertools import count
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from time import sleep
import threading
import time
from tkinter import *

#Alle globale variabelen worden hier voor het eerst defined.
root = Tk()

einddata = ""
x_waardes = [0]
y_waardesV = [0]
y_waardesQ = [0]
y_waardesN = [0]
y_waardesX = [0]
y_waardesM = [0]

status = 0

snelheid = 0
vermogenbelasting = 0
rendementaandrijving = 0
omgevingstemperatuur = 0
belastingtemperatuur = 0
belastingkoppel = 0
aandrijvingvermogen = 0
rendementbelasting = 0

index = count(0)

#Deze functie zorgt ervoor dat de grafieken live aanpasbaar zijn in de code.
plt.ion()

#Deze thread zorgt ervoor dat de waardes in de GUI live geupdate worden afhankelijk van de inkomende payload.
def showwaardes():
    print("Ik kom in showwaardes")
    
    global omgevingstemperatuur
    global belastingtemperatuur
    global belastingkoppel
    
    while(1):
        omgtemperatuurval.config(text = omgevingstemperatuur)
        beltemperatuurval.config(text = belastingtemperatuur)
        koppelbelastingval.config(text = belastingkoppel)
        sleep(1)

#Deze thread ontvangt een payload. Alle headers en junk van de payload wordt er vervolgens uit gefilterd.
#Wat overblijft wordt in einddata gezet.
def wlancom():
    global einddata
    
    print("Geen vastlopen bij thread creatie gevonden.")

    HOST = '0.0.0.0' # Zorg ervoor dat elk IP-adres geaccepteert wordt
    PORT = 2000 # De port om op te luisteren
 
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
     
        while True:
            conn, addr = s.accept()
            with conn:
                print('Connected by', addr)
                #conn.sendall
             
                while True:
                    data = conn.recv(100)
                    updatedata = data.decode('Cp1252')
                 
                    einde = ""
                    i = 0
                    einddata = ""
                    while (einde != "\x00" ):
                        einde = updatedata[i]
                     
                        if (einde != "\x00"):
                            einddata = einddata + updatedata[i]
                            i = i + 1
                 
                    print(einddata)
                    onderscheid()

#Alle grafieken worden hieronder behandelt en weergegeven.
#Elke grafiek heeft een eigen kleur en naam zodat ze allemaal onderscheiden kunnen worden.
def showgrafiek():
    #Check print om te achterhalen of de thread succesvol is gestart.
    print("De grafieken zijn nu te zien.")
    
    #Maak alle variabelen global zodat ze te bereiken zijn door de thread.
    global x_waardes
    global snelheid
    global y_waardesV
    global y_waardesQ
    global vermogenbelasting
    global rendementaandrijving
    global y_waardesN
    global y_waardesX
    global aandrijvingvermogen
    global y_waardesM
    global rendementbelasting
    
    while(1):
        
        x_waardes.append(next(index))
            
        #----------------------------------------------------------------------------
        #Grafiek 1
        y_waardesV.append(snelheid)
            
        shiftleft(x_waardes, y_waardesV)
            
        plt.figure(1)
        
        plt.suptitle("Snelheid", fontsize = 16)
        
        plt.cla() #Clear het vorige plot zodat er niet een nieuw plot wordt gemaakt.

        plt.plot(x_waardes, y_waardesV, color = 'blue') #Plot de grafiek.
        
        plt.draw()

        plt.pause(0.0000001) 

        plt.show()

        #----------------------------------------------------------------------------
        #Grafiek 2
        y_waardesQ.append(vermogenbelasting)
          
        shiftleft(x_waardes, y_waardesQ)
          
        plt.figure(2)
        
        plt.suptitle("Vermogen van de belasting", fontsize = 16)
        
        plt.cla() #Clear het vorige plot zodat er niet een nieuw plot wordt gemaakt.

        plt.plot(x_waardes, y_waardesQ, color = 'red') #Plot de grafiek.
        
        plt.draw()
          
        plt.pause(0.0000001) 
          
        plt.show()
          
        #----------------------------------------------------------------------------
        #Grafiek 3
        y_waardesN.append(rendementaandrijving)
          
        shiftleft(x_waardes, y_waardesN)
          
        plt.figure(3)
        
        plt.suptitle("Rendement van de aandrijving", fontsize = 16)
        
        plt.cla() #Clear het vorige plot zodat er niet een nieuw plot wordt gemaakt.

        plt.plot(x_waardes, y_waardesN, color = 'green') #Plot de grafiek.
        
        plt.draw()

        plt.pause(0.0000001) 

        plt.show()
        #-----------------------------------------------------------------------------
        #Grafiek 4
        y_waardesX.append(aandrijvingvermogen)
          
        shiftleft(x_waardes, y_waardesX)
          
        plt.figure(4)
        
        plt.suptitle("Vermogen van de aandrijving", fontsize = 16)
        
        plt.cla() #Clear het vorige plot zodat er niet een nieuw plot wordt gemaakt.

        plt.plot(x_waardes, y_waardesX, color = 'purple') #Plot de grafiek.
        
        plt.draw()

        plt.pause(0.0000001) 

        plt.show()
        #-----------------------------------------------------------------------
        #Grafiek 5
        y_waardesM.append(rendementbelasting)
          
        shiftleft(x_waardes, y_waardesM)
          
        plt.figure(5)
        
        plt.suptitle("Rendement van de belasting", fontsize = 16)
        
        plt.cla() #Clear het vorige plot zodat er niet een nieuw plot wordt gemaakt.

        plt.plot(x_waardes, y_waardesM, color = 'yellow') #Plot de grafiek.
        
        plt.draw()

        plt.pause(0.0000001) 

        plt.show()
        #-----------------------------------------------------------------------
        #Laat de thread wachten zodat de X-as precies 1 seconde verder gaat bij elke re-plot.
        #Deze waarde is niet 1 want het plotten van de andere grafieken kost ook tijd.
        sleep(0.05)

#Deze functie zorgt ervoor dat de grafiek een niet te lange x-as krijgt.
#Zodra je 10 x coördinaten ziet gaat je view van de grafiek mee naar rechts.
#Dit zorgt er alleen wel voor dat je de vorige waarden niet meer ziet.
def shiftleft(array1, array2):
    if (len(array1) == 11):
        del array1[0]
    if (len(array2) == 11):
        del array2[0]

#Zodra de payload is ontvangen en gefilterd wordt deze functie aangeroepen.
#Deze functie zorgt ervoor dat we afhankelijk van het ID-char weten welke waarde is overgestuurd.
#Elke waarde krijgt respectievelijk zijn eigen variabele.
def onderscheid(): #Deze functie wordt herhaald wanneer er een waarde is binnen gekomen via de poort
    global snelheid
    global vermogenbelasting
    global rendementaandrijving
    global omgevingstemperatuur
    global belastingtemperatuur
    global belastingkoppel
    global aandrijvingvermogen
    global rendementbelasting
    global einddata
    
    if (einddata[0] == 'V'):
        snelheid = einddata[1:]
    
    elif (einddata[0] == 'Q'):
        vermogenbelasting = einddata[1:]
        
    elif (einddata[0] == 'N'):
        rendementaandrijving = einddata[1:]
        
    elif (einddata[0] == 'X'):
        aandrijvingvermogen = einddata[1:]
        
    elif (einddata[0] == 'M'):
        rendementbelasting = einddata[1:]
        
    elif (einddata[0] == 'Z'):
        omgevingstemperatuur = einddata[1:]
        
    elif (einddata[0] == 'A'):
        belastingtemperatuur = einddata[1:]
    
    elif (einddata[0] == 'K'):
        belastingkoppel = einddata[1:]

#Hieronder wordt de tekst GUI gemaakt.
#Alle te weergeven waardes die niet in een grafiek moeten komen krijgt men te zien als normaal getal.
root.title("Live Waardes")
root.geometry("700x200")

omgtemperatuurtxt = Label(root, text= "Omgevings Temperatuur:", width = 30, anchor = "e")
omgtemperatuureen = Label(root, text= "°C", width = 30, anchor = "w")
omgtemperatuurval = Label(root, text= "0", width = 30)
omgtemperatuurtxt.grid(row = 0, column = 0)
omgtemperatuureen.grid(row = 0, column = 2)
omgtemperatuurval.grid(row = 0, column = 1)

beltemperatuurtxt = Label(root, text= "Belastings Temperatuur:", width = 30, anchor = "e")
beltemperatuureen = Label(root, text= "°C", width = 30, anchor = "w")
beltemperatuurval = Label(root, text= "0", width = 30)
beltemperatuurtxt.grid(row = 1, column = 0)
beltemperatuureen.grid(row = 1, column = 2)
beltemperatuurval.grid(row = 1, column = 1)

koppelbelastingtxt = Label(root, text="Koppel:", width = 30, anchor = "e")
koppelbelastingeen = Label(root, text="Ncm", width = 30, anchor = "w")
koppelbelastingval = Label(root, text= "0", width = 30)
koppelbelastingtxt.grid(row = 2, column = 0)
koppelbelastingeen.grid(row = 2, column = 2)
koppelbelastingval.grid(row = 2, column = 1)

#Hieronder worden de threads gemaakt.
#Threads zorgen ervoor dat je de onderstaande functies tegelijk kan uitvoeren.
thread2 = threading.Thread(target=wlancom)
thread1 = threading.Thread(target=showgrafiek)
thread3 = threading.Thread(target=showwaardes)
thread2.start()
thread1.start()
thread3.start()

#De GUI wordt hier in een loop gezet zodat live interactie met de GUI mogelijk is.
root.mainloop()